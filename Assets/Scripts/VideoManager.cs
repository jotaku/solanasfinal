﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour {


	public GameObject player;
	public GameObject canvasMenu;
	public GameObject video;

	public Material defaultMat;
	public Material skyboxMat;

	bool playing = false;

	void Update()
	{
		if (playing && video.GetComponent<VideoPlayer> ().frame == (long)video.GetComponent<VideoPlayer> ().frameCount){
			video.SetActive (false);
			canvasMenu.SetActive (true);

			RenderSettings.skybox = skyboxMat;
			playing = false;
		}
	}


	public void GoVideo()
	{
		RenderSettings.skybox = defaultMat;
		canvasMenu.SetActive (false);

		video.SetActive (true);
		video.GetComponent<VideoPlayer> ().Play ();
		StartCoroutine (PlayVideo());
	}

	IEnumerator PlayVideo(){
		yield return  new WaitForSeconds (3);
		playing = true;
	}

}
