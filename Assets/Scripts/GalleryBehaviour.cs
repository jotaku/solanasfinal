﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GalleryBehaviour : MonoBehaviour {

	public RectTransform gallery; 
	public float maxMov;
	public float minMov;
	public List<Image> images;
	public List<Image> dots;
	public Sprite currentDot;
	public Sprite no_currentDot;

	[SerializeField]
	int currentImage = 6;
	[SerializeField]
	float centerImageAlpha;
	[SerializeField]
	float nextTocenterImageAlpha;
	[SerializeField]
	float restAlpha;
	[SerializeField]
	int moveX;

	void Start()
	{
	}

	public void Back() //=>
	{
		
		if(gallery.localPosition.x < maxMov)
		{
			gallery.localPosition = new Vector2 (gallery.localPosition.x + moveX, gallery.localPosition.y);

			ChangeAlpha (images [currentImage],nextTocenterImageAlpha);
			if (currentImage + 1 < images.Count) {
				ChangeAlpha (images [currentImage + 1], restAlpha);
			} else {
				
			}
			currentImage--; //me muevo uno
			SetDot (currentImage);
			if (currentImage - 1 >= 0) {
				ChangeAlpha (images [currentImage - 1], nextTocenterImageAlpha);
			}
			ChangeAlpha (images [currentImage],centerImageAlpha);
		}
	}

	public void Next() //=>
	{
		
		if(gallery.localPosition.x > minMov)
		{
			gallery.localPosition = new Vector2 (gallery.localPosition.x - moveX, gallery.localPosition.y);

			ChangeAlpha (images [currentImage],nextTocenterImageAlpha);
			if(currentImage - 1 > 0){
				ChangeAlpha (images [currentImage-1],restAlpha);
			}
			currentImage++;
			SetDot (currentImage);
			if (currentImage + 1 < images.Count) {
				ChangeAlpha (images [currentImage + 1], nextTocenterImageAlpha);
			}
			ChangeAlpha (images [currentImage], centerImageAlpha);
		}
	}

	void ChangeAlpha(Image i, float newAlpha)
	{
		//i.color = new Color(i.color.r,i.color.g,i.color.b,newAlpha);

		var color = i.color;
		color.r = i.color.r;
		color.g = i.color.g;
		color.b = i.color.b;
		color.a = newAlpha;
		i.color = color;

	}

	void SetDot(int current){
		for (int i = 0; i < dots.Count; i++) {
			dots [i].GetComponent<Image> ().overrideSprite = no_currentDot;
		}
		dots [current].GetComponent<Image> ().overrideSprite = currentDot;
	}
}
