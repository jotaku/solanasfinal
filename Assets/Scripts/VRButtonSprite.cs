﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRButtonSprite : MonoBehaviour {

	public float overTime = 0;
	public float clickTime = 3;

	bool clicked = false;

	public bool over;

	public GameObject loading;
	public GalleryManager galleryManager;
	public int imageIndex;
	public GameObject nextButton;
	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {


		if (over) {
			loading.SetActive (true);
			overTime += Time.deltaTime;

			if ((overTime >= clickTime)&&(!clicked))
			{
				Click ();
				over = false;
			}
		} else {
			overTime = 0;

			loading.SetActive (false);
		}
	}

	public void setOver(bool over){
		this.over = over;
	}

	public void Click()
	{
		galleryManager.GoToImage (imageIndex);
		this.gameObject.SetActive (false);
		nextButton.SetActive (true);
	}
}
