﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRButton : MonoBehaviour {

	public float overTime = 0;
	public float clickTime = 3;

	bool clicked = false;

	public bool over;

	public bool galleryButton;
	public GameObject pointer;
	public GameObject loading;
	public Material active;
	public Material inactive;
	public GameObject usingSprite;
	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {


		if (over) {
			//loading.SetActive (true);
			pointer.GetComponent<MeshRenderer> ().materials[0] = active;
			usingSprite.SetActive (true);
			overTime += Time.deltaTime;

			if ((overTime >= clickTime)&&(!clicked))
			{
				//do wathever you want.
				//MainBehaviour();
				if (galleryButton) {
					StartCoroutine (Clicking ());
				} else {
					this.GetComponent<Button> ().onClick.Invoke ();
				}
				overTime = 0;
				pointer.GetComponent<MeshRenderer> ().materials[0] = inactive;
				usingSprite.SetActive (false);
				over = false;
			}
		} else {
			overTime = 0;
			pointer.GetComponent<MeshRenderer> ().materials[0] = inactive;
			usingSprite.SetActive (false);
			//loading.SetActive (false);
		}
	}

	public void setOver(bool over){
		this.over = over;
	}

	IEnumerator Clicking(){
		//this.GetComponent<Animator> ().SetBool ("click",true);
		yield return new WaitForSeconds (1);
		//this.GetComponent<Animator> ().SetBool ("click",false);
		this.GetComponent<Button>().onClick.Invoke();
	}
}
