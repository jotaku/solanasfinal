﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GalleryImageBehaviour : MonoBehaviour {

	public int id;
	GalleryManager galleryManager;

	void Start()
	{
		galleryManager = GameObject.FindGameObjectWithTag ("SkyboxManager").GetComponent<GalleryManager> ();
	}

	public void Click()
	{
		//galleryManager.GoGallery ();
		RenderSettings.skybox = galleryManager.skyboxMat;
		galleryManager.skyboxMat.mainTexture = galleryManager.textures [id];
	}
}
