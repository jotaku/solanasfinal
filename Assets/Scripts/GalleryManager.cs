﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GalleryManager : MonoBehaviour {

	public GameObject player;
	public GameObject gallery;
	public GameObject focusPanel;
	public Material defaultMat;
	public Material skyboxMat;
	public List<Texture> textures;

	public float time;

	public bool isReel;

	Quaternion initialRotation;

	public Texture initial;

	public OVRScreenFade fade;
	// Use this for initialization
	void Start () {
		initialRotation = player.transform.localRotation;
		//RenderSettings.skybox = defaultMat;
		if (isReel) {
			ReelImages ();
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	}



	IEnumerator ReelRoutine(){
		for (int i = 0 ; i  < textures.Count - 1 ; i++) {
			Debug.Log ("Cambio textura");
			player.transform.localRotation = initialRotation;
			skyboxMat.mainTexture = textures [i];
			yield return new WaitForSeconds (time);
		}

	}

	public void ReelImages(){
		gallery.SetActive (false);
		RenderSettings.skybox = skyboxMat;
		StartCoroutine (ReelRoutine());
	}

	bool galleryDown = true;
	public void GoGallery(){
		focusPanel.SetActive (false);
		gallery.SetActive (true);
	}

	public void GoToImage(int i){
		StartCoroutine(transition(i));
	}

	IEnumerator transition(int index){
		
			for (int i = 0; i < 10; i++) {
				skyboxMat.SetFloat ("_Exposure", (1f - (0.1f * i)));
				yield return new WaitForSeconds (0.05f);
			}
		
		yield return new WaitForSeconds (0.1f);
		skyboxMat.mainTexture = textures [index];
		for (int i = 0; i < 10; i++) {
			skyboxMat.SetFloat ("_Exposure", ( 0.1f * i));
			yield return new WaitForSeconds (0.05f);
		}
	}

	public void BlackBackground(){
		//0 0 0 255
		skyboxMat.SetColor ("_TintColor", Color.black);
	}
	public void GreyBackground(){
		//113 113 113 255 new Color(113,113,133,255)
		skyboxMat.SetColor ("_TintColor", Color.grey);
	}

	public void SetNullTexture(){
		skyboxMat.mainTexture = initial;
	}



	public void Fade(){
		fade.StartFade (1,0);
	}

}
