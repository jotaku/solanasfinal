﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class FixResolution : MonoBehaviour {

	// Use this for initialization
	void Start () {
        UnityEngine.XR.XRSettings.eyeTextureResolutionScale = 2.0f;
        //XRSettings.eyeTextureResolutionScale = 2.0f; //replaces the deprecated VRSettings.renderScale

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
