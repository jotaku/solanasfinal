﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowButton : MonoBehaviour {


	public GameObject arrow1;
	public GameObject arrow2;

	float time = 0;
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;
		if (time >= 0.5f) {
			arrow2.SetActive (true);
			time = 0;
		}
	}
}
